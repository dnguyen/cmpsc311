#!/bin/bash

CLT_LOG=clt-output.log
SERV_LOG=server-output.log

echo -e "\n[SIMPLE.dat]\n"
> $SERV_LOG
rm $CLT_LOG
./smsaclt simple.dat -l $CLT_LOG

./verify simple-output.log $CLT_LOG $SERV_LOG

echo -e "\n==============================================="
echo -e "LINEAR.dat\n" 
> $SERV_LOG
rm $CLT_LOG
./smsaclt linear.dat -l $CLT_LOG

./verify linear-output.log  $CLT_LOG $SERV_LOG

echo -e "\n==============================================="
echo -e "RANDOM.dat\n" 
> $SERV_LOG
rm $CLT_LOG
./smsaclt random.dat -l $CLT_LOG

./verify random-output.log  $CLT_LOG $SERV_LOG

echo -e "\n==============================================="
echo -e "REFLOC.dat\n"
> $SERV_LOG
rm $CLT_LOG
./smsaclt refloc.dat -l $CLT_LOG

./verify refloc-output.log $CLT_LOG $SERV_LOG

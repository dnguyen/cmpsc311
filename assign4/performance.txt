Simple.dat
+------------+---------+
| Cache Size | Cycles  |
+------------+---------+
|         32 | 3319890 |
|         64 | 3399090 |
|        256 | 3393930 |
|       1024 | 3376710 |
|       2048 | 3369630 |
|       4096 | 3369630 |
+------------+---------+

Linear.dat
+------------+----------+
| Cache Size |  Cycles  |
+------------+----------+
|         32 | 15744110 |
|         64 | 15732970 |
|        256 | 15645060 |
|       1024 | 15324750 |
|       2048 | 14914550 |
|       4096 | 14362420 |
+------------+----------+

Random.dat
+------------+----------+
| Cache Size |  Cycles  |
+------------+----------+
|         32 | 47076320 |
|         64 | 47057260 |
|        256 | 46944670 |
|       1024 | 46481540 |
|       2048 | 45878330 |
|       4096 | 44653040 |
+------------+----------+

The amount of cycles decreases as the size of the cache is increased. With a larger cache you can store more drums/blocks in the cache which means the disk does less reads.

#!/bin/bash

echo "Testing simple.dat"
rm cmpsc311.log
./smsasim simple.dat -l cmpsc311.log
./verify simple-output.log cmpsc311.log

echo "Testing linear.dat"
rm cmpsc311.log
./smsasim linear.dat -l cmpsc311.log
./verify linear-output.log cmpsc311.log

echo "Testing random.dat"
rm cmpsc311.log
./smsasim random.dat -l cmpsc311.log
./verify random-output.log cmpsc311.log

#include <stdio.h>
#include <a2support.h>

////////////////////////////////////////////////////////////////////////////////
//
// Function     : printCharline
// Description  : Prints a character a certain amount of times. 
//               
//
// Inputs       : Character to print, how many times to print
// Outputs      : none
void printCharline(char character, int count) {
    int i;

    for (i = 0; i < count; i++) {
        if (count <= 80) {
            printf("%c", character);
        }
    }

    printf("\n");
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : myRound
// Description  : Rounds a float to an integer. When decimal value is above .5
//                it will round up.
//
// Inputs       : Float value
// Outputs      : Integer value
int myRound(float value) {
    return (int) (value + 0.5);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : printFArray
// Description  : Prints a float array
//
// Inputs       : Reference to float  array, length of the array
// Outputs      : none
void printFArray(float *fArray, int length) {
    int i;

    for (i = 0; i < length; i++) {
       printf("%6.2f ", fArray[i]);
    }

    printf("\n");
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : printIArray
// Description  : Prints an integer array
//
// Inputs       : Reference to integer array, length of the array
// Outputs      : none
void printIArray(int *iArray, int length) {
    int i;
    for (i = 0; i < length; i++) {
        printf("%6d ", iArray[i]);
    }

    printf("\n");
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : incrementFloat
// Description  : Increments a float by a float  value
//
// Inputs       : Reference to a float value, integer value to increment by
// Outputs      : none
void incrementFloat(float *value,float incrementBy) {
    *value += incrementBy;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : incrementInt
// Description  : Increments an integer by a value
//
// Inputs       : Reference to an integer value, integer value to increment by
// Outputs      : none
void incrementInt(int *value, int incrementBy) {
    *value += incrementBy;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : largestFloat
// Description  : Finds the largest float in a float array.
//
// Inputs       : Reference to float array, length of the array
// Outputs      : Largest float
float largestFloat(float *fArray, int length) {
    float currentLargest = fArray[0];
    int i;
    
    for (i = 0; i < length; i++) {
        if (fArray[i] > currentLargest) {
            currentLargest = fArray[i];
        }
    }

    return currentLargest;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : largestInt
// Description  : Finds the largest integer in an integer  array.
//
// Inputs       : Reference to int array, length of the array
// Outputs      : Largest integer
int largestInt(int *iArray, int length) {
    int currentLargest = iArray[0];
    int i;

    for (i = 0; i < length; i++) {
        if (iArray[i] > currentLargest) {
            currentLargest = iArray[i];
        }
    }

    return currentLargest;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : bsortFloat
// Description  : Sorts the values of an array of floats using a bubble sort.
//
// Inputs       : Reference to float array, length of the array
// Outputs      : none
void bsortFloat(float *fArray, int length) {
    int swapped;
    int i;
    float temp;

    // Keep going through the array until no swaps are made.
    do {
        swapped = 0;
        for (i = 1; i <= length - 1; i++) {
            // Swap the  elements if the element before the current element is larger than the current element.
            if (fArray[i-1] > fArray[i]) {
                // Do swap
                temp = fArray[i-1];
                fArray[i-1] = fArray[i];
                fArray[i] = temp;
                swapped = 1;
            }
        }

    } while (swapped == 1);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : bsortInt
// Description  : Sorts the values of an array of integers using a bubble sort.
//
// Inputs       : Reference to int array, length of the array
// Outputs      : none
void bsortInt(int *iArray, int length) {
    int swapped;
    int i;
    int temp;

    // Keep going through the array until no swaps are made.
    do {
        swapped = 0;
        for (i = 1; i <= length - 1; i++) {
            // Swap the  elements if the element before the current element is larger than the current element.
            if (iArray[i-1] > iArray[i]) {
                // Do swap
                temp = iArray[i-1];
                iArray[i-1] = iArray[i];
                iArray[i] = temp;
                swapped = 1;
            }
        }

    } while (swapped == 1);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : getBinIndexForInt
// Description  : Determines what bin an integer value belongs to.
//
// Inputs       : Integer being checked.
// Outputs      : Returns the integer index 
int getBinIndexForInt(int value) {
    int binIndex = 0;

    if (value < 50) {
        binIndex = 0;
    } else if (value >= 50 && value < 100) {
        binIndex = 1;
    } else if (value >= 100 && value < 150) {
        binIndex = 2;
    } else if (value >= 150 && value < 200) {
        binIndex = 3;
    } else if (value >= 200 && value < 250) {
        binIndex = 4;
    } else if (value >= 250 && value < 300) {
        binIndex = 5;
    } else if (value >= 300 && value < 350) {
        binIndex = 6;
    } else if (value >= 350 && value < 400) {
        binIndex = 7;
    } else if (value >= 400 && value < 450) {
        binIndex = 8;
    } else if (value >= 450) {
        binIndex = 9;
    }

    return binIndex;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : getBinIndexForFloat
// Description  : Determines what bin a float value belongs to.
//
// Inputs       : Float being checked.
// Outputs      : Returns the integer index 
int getBinIndexForFloat(float value) {
    int binIndex = 0;

    if (value < 50) {
        binIndex = 0;
    } else if (value >= 50 && value < 100) {
        binIndex = 1;
    } else if (value >= 100 && value < 150) {
        binIndex = 2;
    } else if (value >= 150 && value < 200) {
        binIndex = 3;
    } else if (value >= 200 && value < 250) {
        binIndex = 4;
    } else if (value >= 250 && value < 300) {
        binIndex = 5;
    } else if (value >= 300 && value < 350) {
        binIndex = 6;
    } else if (value >= 350 && value < 400) {
        binIndex = 7;
    } else if (value >= 400 && value < 450) {
        binIndex = 8;
    } else if (value >= 450) {
        binIndex = 9;
    }

    return binIndex;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : doHistogram
// Description  : Prints a histogram for an integer array and a float array.
//
// Inputs       : Reference to integer array, reference to float array, length of integer array,
//                length of float array
// Outputs      : none
void doHistogram(int *iArray, float *fArray, int intLength, int floatLength) {
    // Create array to store number of values in each bin.
    // Each element represents one of the x-axis values / bins (<50, 50+, 100+, etc)
    const int NUMBER_BINS = 10;
    int binCounts[NUMBER_BINS];
    int i;

    // Initialize bin array values  to 0
    for (i = 0; i < NUMBER_BINS; i++) {
        binCounts[i] = 0;
    }

    // Loop through both the float array and integer array
    // And determine which bins each integer and float belongs to.
    for (i = 0; i < intLength; i++) {
        binCounts[getBinIndexForInt(iArray[i])]++;
    }

    for (i = 0; i < floatLength; i++) {
        binCounts[getBinIndexForFloat(fArray[i])]++;
    }

    printf("\n");

    // Use the size of the largest bin to get the maximum y-axis value.
    int maxYValue = largestInt(binCounts, NUMBER_BINS);
    int y;

    // Start printing histogram line by line using maxYValue
    for (y = maxYValue + 1; y >= 1; y--) {

        // Print current y-axis value
        printf("%2d ", y);

        // Determine if an * or empty space should be printed
        // Loop through each bin
        for (i = 0; i < NUMBER_BINS; i++) {
            // If the size of the current bin is larger than the current y-axis value - 1,
            // then print an *, else print empty space.
            if (binCounts[i] > y-1) {
                printf("%6c", '*');
            } else {
                printf("%6c", ' ');
            }
        }

        // Finished printing 1 line of the histogram.
        printf("\n");
    }

    // Print the x-axis values
    int xvalue = 50; // x-axis values start at 50

    // Loop through each bin
    for (i = 0; i < NUMBER_BINS; i++) {
        // Print "<50" for first bin
        if (i == 0) {
            printf("%9s", "<50"); 

        // Print "050+" for second bin
        } else if (i == 1) {
            printf("%6s", "050+");

        // Print ">=450" for the last bin.
        } else if (i == 9) {
            printf("%6s", ">=450");

        // Print "xvalue+" for the rest of the bins
        } else {
            printf("%5d", xvalue);
            printf("%c", '+');
        }
        xvalue += 50;
    }

    printf("\n");
}


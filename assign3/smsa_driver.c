////////////////////////////////////////////////////////////////////////////////
// 
//  File           : smsa_driver.c
//  Description    : This is the driver for the SMSA simulator.
//
//   Author        : Dylan Nguyen
//   Last Modified : 10/21/2013
//

// Include Files

// Project Include Files
#include <smsa_driver.h>
#include <cmpsc311_log.h>

// Defines

// Functional Prototypes

//
// Global data

// Interfaces

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_vmount
// Description  : Mount the SMSA disk array virtual address space
//
// Inputs       : none
// Outputs      : -1 if failure or 0 if successful

int smsa_vmount( void ) {
    return smsa_operation(generateOpCode(SMSA_MOUNT, 0, 0), NULL);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_vunmount
// Description  :  Unmount the SMSA disk array virtual address space
//
// Inputs       : none
// Outputs      : -1 if failure or 0 if successful

int smsa_vunmount( void )  {
    return smsa_operation(generateOpCode(SMSA_UNMOUNT, 0, 0), NULL);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_vread
// Description  : Read from the SMSA virtual address space
//
// Inputs       : addr - the address to read from
//                len - the number of bytes to read
//                buf - the place to put the read bytes
// Outputs      : -1 if failure or 0 if successful

int smsa_vread( SMSA_VIRTUAL_ADDRESS addr, uint32_t len, unsigned char *buf ) {
    // Translate the addr into a drum id, block id, and offset
    SMSA_DRUM_ID drumId = getDrumId(addr);
    SMSA_BLOCK_ID blockId = getBlockId(addr);
    int offset = getOffset(addr);
    
    // Store the bytes that are read from the disk into a buffer
    unsigned char tempBuffer[SMSA_BLOCK_SIZE];

    int readBytes = 0;

    int errors = 0;

    SMSA_BLOCK_ID currentBlockId = blockId;
    SMSA_DRUM_ID currentDrumId = drumId;
    SMSA_BLOCK_ID blocksRead = 0;

    // Move head to our starting drum and block
    smsa_operation(generateOpCode(SMSA_SEEK_DRUM, drumId, 0), NULL);
    smsa_operation(generateOpCode(SMSA_SEEK_BLOCK, 0, blockId), NULL);

    // Keep reading and copying bytes until we have read all the bytes up to len
    do {
        // Keep track of where we're at.
        currentBlockId = getBlockId(addr + readBytes);
        currentDrumId = getDrumId(addr + readBytes);  

        // Make sure the current block and current drum that we're trying to read are valid
        if (isValidBlock(currentBlockId) == -1 || isValidDrum(currentDrumId) == -1) {
            errors = 1;
            break;
        }

        // Reads all 256 bytes from the blockId into tempBuffer
        smsa_operation(generateOpCode(SMSA_DISK_READ, 0, 0), tempBuffer);

        // Determine if we need to start copying from the middle of the buffer
        // or from the beginning of the buffer
        int i;
        if (blocksRead == 0) {
            i = offset;
        } else {
            i = 0;
        }
        // Start copying read bytes into buf
        // Only copies a max of 256 bytes into the buffer at a time.
        // If we weren't able to fit all the bytes that need to be read into the buffer,
        //      Next pass through start copying bytes from the offset
        do {
            buf[readBytes] = tempBuffer[i];
            readBytes++;
            i++;
        } while (i < SMSA_BLOCK_SIZE && readBytes < len);
        // Finished reading 256 bytes of the current block
        
        // Since we're are done reading the current block,
        //      Seek to the next drum if we're on the last block of a drum.
        if (currentBlockId >= SMSA_BLOCK_SIZE - 1 && currentDrumId < SMSA_DISK_ARRAY_SIZE - 1) {
            generateOpCode(SMSA_SEEK_DRUM, currentDrumId + 1, 0);
            smsa_operation(generateOpCode(SMSA_SEEK_DRUM, currentDrumId + 1, 0), NULL);
            currentBlockId = 0;
        }

        blocksRead++;

    } while (readBytes < len);

    if (errors == 0) {
        return 0;
    } else {
        return -1;
    }
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_vwrite
// Description  : Write to the SMSA virtual address space
//
// Inputs       : addr - the address to write to
//                len - the number of bytes to write
//                buf - the place to read the read from to write
// Outputs      : -1 if failure or 0 if successful

int smsa_vwrite( SMSA_VIRTUAL_ADDRESS addr, uint32_t len, unsigned char *buf )  {
    logMessage(LOG_INFO_LEVEL, "STARTING WRITE");
    // Store the bytes that were read from the disk in a buffer. (readData)
    unsigned char readData[SMSA_BLOCK_SIZE];

    uint32_t bytesWritten = 0;
    SMSA_DRUM_ID drumId = getDrumId(addr);
    SMSA_BLOCK_ID blockId = getBlockId(addr);
    uint32_t offset = getOffset(addr);

    SMSA_BLOCK_ID currentBlockId = blockId;
    SMSA_DRUM_ID currentDrumId = drumId;
    uint32_t currentOffset = offset;

    SMSA_BLOCK_ID blocksWritten = 0;

    // Keep writing until the total bytes written is equal to the len
    do {
        // Translate the current address into a drum, block, and offset
        currentDrumId = getDrumId(addr + bytesWritten);
        currentBlockId = getBlockId(addr + bytesWritten);
        currentOffset = getOffset(addr + bytesWritten);
        
        logMessage(LOG_INFO_LEVEL, "Writing to: addr[%u] len_bytes[%u]. Drum/Block:[%u][%u] - offset:[%u]. Bytes Written:[%u]. Blocks Written:[%u]",
                addr, len, currentDrumId, currentBlockId, currentOffset, bytesWritten, blocksWritten);

        // Make sure the current block and current drum that we're trying to read are valid
        if (isValidBlock(currentBlockId) == -1 || isValidDrum(currentDrumId) == -1) {
            return -1;
        }

        smsa_operation(generateOpCode(SMSA_SEEK_DRUM, currentDrumId, 0), NULL);
        smsa_operation(generateOpCode(SMSA_SEEK_BLOCK, 0, currentBlockId), NULL);

        // Read the current block that we are on to make sure we don't overwrite data from previous writes
        smsa_operation(generateOpCode(SMSA_DISK_READ, 0, 0), readData);

        // Reset head back to the current drum/block that we want to write to.
        smsa_operation(generateOpCode(SMSA_SEEK_DRUM, currentDrumId, 0), NULL);
        smsa_operation(generateOpCode(SMSA_SEEK_BLOCK, 0, currentBlockId), NULL);

        // Build the buffer that needs to be written to the disk
        int i, bufIndex;
        if (blocksWritten == 0) {
            i = currentOffset;
            bufIndex = 0;
        } else {
            i = 0;    
        }

        // Start copying the bytes that we want to write to the disk to the readData buffer
        //      Use readData buffer so we don't overwrite any bytes that were written previously
        do {
            logMessage(LOG_INFO_LEVEL, "WRITING BUFFER: buf[%u]=[%u] to readData[%u]=[%u]", 0, buf[0], i, readData[i]);

            readData[i] = buf[bufIndex];
            bytesWritten++;
            i++;
            bufIndex++;
        } while (i < SMSA_BLOCK_SIZE && bytesWritten < len);

        // Write new read data to disk. 
        smsa_operation(generateOpCode(SMSA_DISK_WRITE, 0, 0), readData);
        logMessage(LOG_INFO_LEVEL, "WRITING READDATA");

        // Check if a full block has been written.
        if (i >= SMSA_BLOCK_SIZE) { 
            blocksWritten++;
            logMessage(LOG_INFO_LEVEL, "FINISHED WRITING WHOLE BLOCK i: [%u]", i);
        }

    } while (bytesWritten < len);
    logMessage(LOG_INFO_LEVEL, "ENDING WRITE");

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : getOffset
// Description  : Gets the offset for an address
//
// Inputs       : addr - the address to get the offset from
// Outputs      : The offset of the address
int getOffset(SMSA_VIRTUAL_ADDRESS addr) {
    return addr % SMSA_BLOCK_SIZE;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : getBlockId
// Description  : Gets the block id for an adddress
//
// Inputs       : addr - the address to get the block id from
// Outputs      : The block id of the address
SMSA_BLOCK_ID getBlockId(SMSA_VIRTUAL_ADDRESS addr) {
    addr = addr & 0x0FF00;
    return addr >> 8;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : getDrumId
// Description  : Gets the drum id for an adddress
//
// Inputs       : addr - the address to get the drum id from
// Outputs      : The drum id of the address
SMSA_DRUM_ID getDrumId(SMSA_VIRTUAL_ADDRESS addr) {
    // Divide by 2^16
    return addr >> SMSA_DISK_ARRAY_SIZE;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : generateOpCode
// Description  : Generates a 32 bit operation. Combines a op code, drum id, and block id.
//
// Inputs       : opcode - The operation that we want
//                drumId - Drum id to operate on
//                blockId - Block id to operate on
// Outputs      : a 32 bit integer 
uint32_t generateOpCode(uint32_t opcode, SMSA_DRUM_ID drumId, SMSA_BLOCK_ID blockId) {
    opcode = opcode << 26;

    // SMSA_BLOCK_ID is a char...so have to cast to an int before shifting.
    uint32_t drumId_int = (uint32_t) drumId;
    drumId_int = drumId_int << 22;

    opcode = opcode | drumId_int;
    opcode = opcode | blockId;

    return opcode;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : isValidDrum
// Description  : Makes sure  a drum id is valid.
// Inputs       : drumId - The drum id that needs to be checked
// Outputs      : 0 if its valid
//              : -1 if it's invalid
int isValidDrum(SMSA_DRUM_ID drumId) {
    if (drumId < 0 || drumId > SMSA_DISK_ARRAY_SIZE) {
        logMessage(LOG_ERROR_LEVEL, "Invalid disk id: [%d]", drumId);
        return -1;
    } else {
        return 0;
    }
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : isValidBlock
// Description  : Makes sure  a block id is valid.
// Inputs       : drumId - The block id that needs to be checked
// Outputs      : 0 if its valid
//              : -1 if it's invalid
int isValidBlock(SMSA_BLOCK_ID blockId) {
    if (blockId < 0 || blockId > SMSA_MAX_BLOCK_ID) {
        logMessage(LOG_ERROR_LEVEL, "Invalid block id: [%d]", blockId);
        return -1;
    } else {
        return 0;
    }
}


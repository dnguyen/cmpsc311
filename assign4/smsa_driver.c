////////////////////////////////////////////////////////////////////////////////
// 
//  File           : smsa_driver.c
//  Description    : This is the driver for the SMSA simulator.
//
//   Author        : Dylan Nguyen
//   Last Modified : 11/16/2013
//

// Include Files
#include <stdlib.h>

// Project Include Files
#include <smsa_driver.h>
#include <smsa_cache.h>
#include <cmpsc311_log.h>

// Defines

// Functional Prototypes
int doSeek(SMSA_DRUM_ID drum, SMSA_BLOCK_ID block);
int doRead(SMSA_DRUM_ID drum, SMSA_BLOCK_ID block, unsigned char **buffer);

//
// Global data

// Keep track of the SMSA head
SMSA_DRUM_ID smsa_current_drum;
SMSA_BLOCK_ID smsa_current_block;

// Interfaces

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_vmount
// Description  : Mount the SMSA disk array virtual address space
//
// Inputs       : none
// Outputs      : -1 if failure or 0 if successful

int smsa_vmount( int lines ) {
    smsa_current_drum = 0;
    smsa_current_block = 0;

    // Check if the cache was successfully initialized
    if (smsa_init_cache(lines) == -1) {
        logMessage(LOG_ERROR_LEVEL, "FAILED TO INITIALIZE CACHE. lines=[%u]", lines);
        return -1;
    } else {
        return smsa_operation(generateOpCode(SMSA_MOUNT, 0, 0), NULL);
    }
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_vunmount
// Description  :  Unmount the SMSA disk array virtual address space
//
// Inputs       : none
// Outputs      : -1 if failure or 0 if successful

int smsa_vunmount( void )  {
    smsa_close_cache();
    return smsa_operation(generateOpCode(SMSA_UNMOUNT, 0, 0), NULL);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_vread
// Description  : Read from the SMSA virtual address space
//
// Inputs       : addr - the address to read from
//                len - the number of bytes to read
//                buf - the place to put the read bytes
// Outputs      : -1 if failure or 0 if successful

int smsa_vread( SMSA_VIRTUAL_ADDRESS addr, uint32_t len, unsigned char *buf ) {
    // Translate the addr into a drum id, block id, and offset
    SMSA_DRUM_ID drumId = getDrumId(addr);
    SMSA_BLOCK_ID blockId = getBlockId(addr);
    uint32_t offset = getOffset(addr);
    
    // Store the bytes that are read from the disk into a buffer
    unsigned char *tempBuffer;

    int readBytes = 0;

    SMSA_BLOCK_ID currentBlockId = blockId;
    SMSA_DRUM_ID currentDrumId = drumId;
    uint32_t currentOffset = offset;

    int blocksRead = 0;

    // Keep reading and copying bytes until we have read all the bytes up to len
    do {
        // Keep track of where we're at.
        currentBlockId = getBlockId(addr + readBytes);
        currentDrumId = getDrumId(addr + readBytes);  
        currentOffset = getOffset(addr + readBytes);

        // Make sure the current block and current drum that we're trying to read are valid
        if (isValidBlock(currentBlockId) == -1 || isValidDrum(currentDrumId) == -1) {
            return -1;
        }

        if (doSeek(currentDrumId, currentBlockId) == -1) {
            return -1;
        }

        tempBuffer = malloc(SMSA_BLOCK_SIZE);
        if (doRead(currentDrumId, currentBlockId, &tempBuffer) == -1) {
            return -1;
        }

        // Determine if we need to start copying from the middle of the buffer
        // or from the beginning of the buffer
        int i, bufIndex;
        if (blocksRead == 0) {
            i = currentOffset;
            bufIndex = 0;
        } else {
            i = 0;
        }
        // Start copying read bytes into buf
        // Only copies a max of 256 bytes into the buffer at a time.
        // If we weren't able to fit all the bytes that need to be read into the buffer,
        //      Next pass through start copying bytes from the offset
        do {
            buf[bufIndex] = tempBuffer[i];
            readBytes++;
            i++;
            bufIndex++;
        } while (i < SMSA_BLOCK_SIZE && readBytes < len);
        // Finished reading 256 bytes of the current block
        
        if (i >= SMSA_BLOCK_SIZE) {
            blocksRead++;
        }

    } while (readBytes < len);

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_vwrite
// Description  : Write to the SMSA virtual address space
//
// Inputs       : addr - the address to write to
//                len - the number of bytes to write
//                buf - the place to read the read from to write
// Outputs      : -1 if failure or 0 if successful

int smsa_vwrite( SMSA_VIRTUAL_ADDRESS addr, uint32_t len, unsigned char *buf )  {
    logMessage(LOG_INFO_LEVEL, "STARTING WRITE");
    // Store the bytes that were read from the disk in a buffer. (readData)
    unsigned char *readData;

    uint32_t bytesWritten = 0;
    SMSA_DRUM_ID drumId = getDrumId(addr);
    SMSA_BLOCK_ID blockId = getBlockId(addr);
    uint32_t offset = getOffset(addr);

    SMSA_BLOCK_ID currentBlockId = blockId;
    SMSA_DRUM_ID currentDrumId = drumId;
    uint32_t currentOffset = offset;

    SMSA_BLOCK_ID blocksWritten = 0;

    // Keep writing until the total bytes written is equal to the len
    do {
        // Translate the current address into a drum, block, and offset
        currentDrumId = getDrumId(addr + bytesWritten);
        currentBlockId = getBlockId(addr + bytesWritten);
        currentOffset = getOffset(addr + bytesWritten);
        
        logMessage(LOG_INFO_LEVEL, "Writing to: addr[%u] len_bytes[%u]. Drum/Block:[%u][%u] - offset:[%u]. Bytes Written:[%u]. Blocks Written:[%u]",
                addr, len, currentDrumId, currentBlockId, currentOffset, bytesWritten, blocksWritten);

        // Make sure the current block and current drum that we're trying to read are valid
        if (isValidBlock(currentBlockId) == -1 || isValidDrum(currentDrumId) == -1) {
            return -1;
        }

        if (doSeek(currentDrumId, currentBlockId) == -1) {
            return -1;
        }

        readData = malloc(SMSA_BLOCK_SIZE);
        if (doRead(currentDrumId, currentBlockId, &readData) == -1) {
            return -1;
        }

        // Build the buffer that needs to be written to the disk
        int i, bufIndex;
        if (blocksWritten == 0) {
            i = currentOffset;
            bufIndex = 0;
        } else {
            i = 0;    
        }

        // Start copying the bytes that we want to write to the disk to the readData buffer
        //      Use readData buffer so we don't overwrite any bytes that were written previously
        do {
            readData[i] = buf[bufIndex];
            bytesWritten++;
            i++;
            bufIndex++;
        } while (i < SMSA_BLOCK_SIZE && bytesWritten < len);
        
        if (doSeek(currentDrumId, currentBlockId) == -1) {
            return -1;
        }

        // Write new read data to the disk and cache. 
        if (smsa_operation(generateOpCode(SMSA_DISK_WRITE, 0, 0), readData) == -1) {
            logMessage(LOG_ERROR_LEVEL, "DISK WRITE FAILED drum[%u]/block[%u]", currentDrumId, currentBlockId);
            return -1;
        }

        smsa_current_block++;
        smsa_put_cache_line(currentDrumId, currentBlockId, readData);

        // Check if a full block has been written.
        if (i >= SMSA_BLOCK_SIZE) { 
            blocksWritten++;
            logMessage(LOG_INFO_LEVEL, "FINISHED WRITING WHOLE BLOCK i: [%u]", i);
        }

    } while (bytesWritten < len);
    logMessage(LOG_INFO_LEVEL, "ENDING WRITE");

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : getOffset
// Description  : Gets the offset for an address
//
// Inputs       : addr - the address to get the offset from
// Outputs      : The offset of the address
int getOffset(SMSA_VIRTUAL_ADDRESS addr) {
    return addr % SMSA_BLOCK_SIZE;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : getBlockId
// Description  : Gets the block id for an adddress
//
// Inputs       : addr - the address to get the block id from
// Outputs      : The block id of the address
SMSA_BLOCK_ID getBlockId(SMSA_VIRTUAL_ADDRESS addr) {
    addr = addr & 0x0FF00;
    return addr >> 8;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : getDrumId
// Description  : Gets the drum id for an adddress
//
// Inputs       : addr - the address to get the drum id from
// Outputs      : The drum id of the address
SMSA_DRUM_ID getDrumId(SMSA_VIRTUAL_ADDRESS addr) {
    // Divide by 2^16
    return addr >> SMSA_DISK_ARRAY_SIZE;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : generateOpCode
// Description  : Generates a 32 bit operation. Combines a op code, drum id, and block id.
//
// Inputs       : opcode - The operation that we want
//                drumId - Drum id to operate on
//                blockId - Block id to operate on
// Outputs      : a 32 bit integer 
uint32_t generateOpCode(uint32_t opcode, SMSA_DRUM_ID drumId, SMSA_BLOCK_ID blockId) {
    opcode = opcode << 26;

    // SMSA_BLOCK_ID is a char...so have to cast to an int before shifting.
    uint32_t drumId_int = (uint32_t) drumId;
    drumId_int = drumId_int << 22;

    opcode = opcode | drumId_int;
    opcode = opcode | blockId;

    return opcode;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : isValidDrum
// Description  : Makes sure  a drum id is valid.
// Inputs       : drumId - The drum id that needs to be checked
// Outputs      : 0 if its valid
//              : -1 if it's invalid
int isValidDrum(SMSA_DRUM_ID drumId) {
    if (drumId < 0 || drumId > SMSA_DISK_ARRAY_SIZE) {
        logMessage(LOG_ERROR_LEVEL, "Invalid disk id: [%d]", drumId);
        return -1;
    } else {
        return 0;
    }
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : isValidBlock
// Description  : Makes sure  a block id is valid.
// Inputs       : drumId - The block id that needs to be checked
// Outputs      : 0 if its valid
//              : -1 if it's invalid
int isValidBlock(SMSA_BLOCK_ID blockId) {
    if (blockId < 0 || blockId > SMSA_MAX_BLOCK_ID) {
        logMessage(LOG_ERROR_LEVEL, "Invalid block id: [%d]", blockId);
        return -1;
    } else {
        return 0;
    }
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : doSeek
// Description  : Seeks to a drum or block if we need to seek.
// Inputs       : drum - drum id
//                block - block id
// Outputs      : 0 if seek was successful
//              : -1 if seek was unsuccessful
int doSeek(SMSA_DRUM_ID drum, SMSA_BLOCK_ID block) {

    // Only need to seek if our driver "head" (smsa_current_drum, smsa_current_block)
    // is out of sync with the SMSA.

    if (smsa_current_drum != drum) {
        if (smsa_operation(generateOpCode(SMSA_SEEK_DRUM, drum, 0), NULL) == -1) {
            logMessage(LOG_ERROR_LEVEL, "SEEK DRUM FAILED");
            return -1;
        }
        smsa_current_drum = drum;
        smsa_current_block = 0;
    }

    if (smsa_current_block != block) {
        if (smsa_operation(generateOpCode(SMSA_SEEK_BLOCK, 0, block), NULL) == -1) {
            logMessage(LOG_ERROR_LEVEL, "SEEK BLOCK FAILED");
            return -1;
        }
        smsa_current_block = block;
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : doRead
// Description  : Reads data from disk (or cache if it exists in cache) into a buffer
// Inputs       : drum - Drum id
//                block - Block id
//                **buffer - Address of buffer to place data in
// Outputs      : 0 if read was successful
//              : -1 if read was unsuccessful
int doRead(SMSA_DRUM_ID drum, SMSA_BLOCK_ID block, unsigned char **buffer) {

    // Check if the drum/block exists in the cache
    // If it doesn't exist, do a disk read and store the drum/block in cache
    // If it does exist, just set the buffer = cache line
    unsigned char *cacheLine = smsa_get_cache_line(drum, block);
    if (cacheLine == NULL) {
        // Do SMSA read
        if (smsa_operation(generateOpCode(SMSA_DISK_READ, 0, 0), *buffer) == -1) {
            logMessage(LOG_ERROR_LEVEL, "DISK READ FAILED");
            return -1;
        }

        smsa_current_block++;
        // Put the read data into the cache
        if (smsa_put_cache_line(drum, block, *buffer) == -1) {
            logMessage(LOG_ERROR_LEVEL, "PUT CACHE LINE IN doRead FAILED");
            return -1;
        }
    } else {
        // To be sure buffer is set to cacheLine correctly.
        if (*buffer != NULL)
            free(*buffer);

        *buffer = NULL;
        *buffer = cacheLine;
    }

    return 0;
}

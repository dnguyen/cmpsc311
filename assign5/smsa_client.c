////////////////////////////////////////////////////////////////////////////////
//
//  File          : smsa_client.c
//  Description   : This is the client side of the SMSA communication protocol.
//
//   Author        : 
//   Last Modified : 
//

// Include Files
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

// Project Include Files
#include <smsa.h>
#include <smsa_network.h>
#include <cmpsc311_log.h>

// Global variables
int SERVER_SOCKET = -1;

// Functional Prototypes
int clientConnect();
int clientDisconnect();
int getOperation(uint32_t);

//
// Functions

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_client_operation
// Description  : This the client operation that sends a reques to the SMSA
//                server.   It will:
//
//                1) if mounting make a connection to the server 
//                2) send any request to the server, returning results
//                3) if unmounting, will close the connection
//
// Inputs       : op - the operation code for the command
//                block - the block to be read/writen from (READ/WRITE)
// Outputs      : 0 if successful, -1 if failure

int smsa_client_operation( uint32_t op, unsigned char *block ) {

    // Figure out what kind of operation we're doing.
    int operation = getOperation(op);

    // If mounting, try to connect to the server
    if (operation == SMSA_MOUNT) {
        logMessage(LOG_INFO_LEVEL, "RECIEVED CLIENT OP: MOUNT");
        // If the client is not connected yet, try to connect.
        if (SERVER_SOCKET == -1) {
            if (clientConnect() == -1) {
                logMessage(LOG_INFO_LEVEL, "FAILED TO CONNECT");
                return -1;
            } else {
                logMessage(LOG_INFO_LEVEL, "CONNECT SUCCESSFUL!");
            }
        }
    }

    // Packet header data
    uint16_t len, ret;
    len = htons(SMSA_NET_HEADER_SIZE);
    op = htonl(op);
    ret = htons(0);

    // How long the packets are. Use in read() and write()
    int packetLen;

    if (operation != SMSA_DISK_WRITE) {
        packetLen = SMSA_NET_HEADER_SIZE;
    } else {
        packetLen = SMSA_NET_HEADER_SIZE + SMSA_BLOCK_SIZE;
        len = htons(SMSA_NET_HEADER_SIZE + SMSA_BLOCK_SIZE);
    }

    // Packet to send to the server
    unsigned char packet[packetLen];

    // Start building the packet to send
    int offset = 0;
    memcpy(&packet[offset], &len, sizeof(uint16_t));
    offset += sizeof(uint16_t);
    memcpy(&packet[offset], &op, sizeof(uint32_t));
    offset += sizeof(uint32_t);
    memcpy(&packet[offset], &ret, sizeof(uint16_t));
    offset += sizeof(uint16_t);

    // If writing, copy block data to the packet
    if (operation == SMSA_DISK_WRITE) {
        memcpy(&packet[offset], block, SMSA_BLOCK_SIZE);
    }

    // Send the packet
    if (write(SERVER_SOCKET, packet, packetLen) == -1) {
        logMessage(LOG_INFO_LEVEL, "FAILED TO WRITE PACKET");
        return -1;
    }

    //
    // Read packet that comes back from the server.
    //
    
    // If reading, packet length = HEADER_SIZE + BLOCK_SIZE
    if (operation == SMSA_DISK_READ) {
        packetLen = SMSA_NET_HEADER_SIZE + SMSA_BLOCK_SIZE;
    } else {
        packetLen = SMSA_NET_HEADER_SIZE;
    }

    // Packet that was recieved
    unsigned char rtrPacket[packetLen];

    // Read the recv packet
    if (read(SERVER_SOCKET, &rtrPacket[0], packetLen) < 0) {
        logMessage(LOG_INFO_LEVEL, "FAILED TO READ PACKET");
        return -1;
    }
    
    // Decode the packet header
    offset = 0;
    memcpy(&len, &rtrPacket, sizeof(uint16_t));
    offset += sizeof(uint16_t);
    len = ntohs(len);
    memcpy(&op, &rtrPacket[offset], sizeof(uint32_t));
    offset += sizeof(uint32_t);
    op = ntohl(op);
    memcpy(&ret, &rtrPacket[offset], sizeof(uint16_t));
    offset += sizeof(uint16_t);
    ret = ntohs(ret);

    logMessage(LOG_INFO_LEVEL, "FINISHED DECODING PACKET");
    logMessage(LOG_INFO_LEVEL, "len:[%d], op:[%d], operation: [%d], ret:[%d]", len, op, getOperation(op), ret);

    // Check return value
    if (ret == -1) {
        logMessage(LOG_ERROR_LEVEL, "Packet RECV error. len=[%d] ret=[%d], op=[%x]", len, ret, op);
        return -1;
    } 

    // If the operation is a read, the server handed the client a buffer,
    // copy the recieved buffer into block.
    if (operation == SMSA_DISK_READ) {
        memcpy(block, &rtrPacket[offset], SMSA_BLOCK_SIZE);
    }
    
    // Disconnect if we're unmounting
    if (operation == SMSA_UNMOUNT) {
        if (clientDisconnect() == -1) {
            return -1;
        }
    }

    return ret;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : clientConnect
// Description  : Connects to the server
//
// Inputs       : none
// Outputs      : 0 for successful, -1 for unsuccessful

int clientConnect() {
    // Setup the address that we are going to connect to.
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(SMSA_DEFAULT_PORT);
    if (inet_aton(SMSA_DEFAULT_IP, &addr.sin_addr) == 0) {
        logMessage(LOG_INFO_LEVEL, "Failed to create the address");
        return -1;
    }
    
    // Create the socket
    SERVER_SOCKET = socket(AF_INET, SOCK_STREAM, 0);
    if (SERVER_SOCKET == -1) {
        logMessage(LOG_INFO_LEVEL, "Failed to create the socket");
        return -1;
    }

    // Try to connect to the server
    if (connect(SERVER_SOCKET, (const struct sockaddr *) &addr, sizeof(struct sockaddr)) == -1) {
        logMessage(LOG_INFO_LEVEL, "Failed to connect.");
        return -1;
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : clientDisconnect
// Description  : Disconnect's the client
//
// Inputs       : none
// Outputs      : 0 for successful

int clientDisconnect() {
    close(SERVER_SOCKET);
    SERVER_SOCKET = -1;

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : getOperation
// Description  : Returns the operation of a given op.
//
// Inputs       : 32 bit op - the operation code for the command
// Outputs      : integer op code

int getOperation(uint32_t opCode) {
    return opCode >> 26;
}



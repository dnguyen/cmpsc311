#!/bin/bash

# SIMPLE.DAT

echo "Testing simple.dat - Cache 32"
rm cmpsc311.log
./smsasim -c 32 simple.dat -l cmpsc311.log
./verify simple-output.log cmpsc311.log

echo "Testing simple.dat - Cache 64"
rm cmpsc311.log
./smsasim -c 64 simple.dat -l cmpsc311.log
./verify simple-output.log cmpsc311.log

echo "Testing simple.dat - Cache 256"
rm cmpsc311.log
./smsasim -c 256 simple.dat -l cmpsc311.log
./verify simple-output.log cmpsc311.log

echo "Testing simple.dat - Cache 1024"
rm cmpsc311.log
./smsasim simple.dat -l cmpsc311.log
./verify simple-output.log cmpsc311.log

echo "Testing simple.dat - Cache 2048"
rm cmpsc311.log
./smsasim -c 2048 simple.dat -l cmpsc311.log
./verify simple-output.log cmpsc311.log

echo "Testing simple.dat - Cache 4096"
rm cmpsc311.log
./smsasim -c 4096 simple.dat -l cmpsc311.log
./verify simple-output.log cmpsc311.log

echo "=========================================\n"
# LINEAR.DAT


echo "Testing linear.dat - Cache 32"
rm cmpsc311.log
./smsasim -c 32 linear.dat -l cmpsc311.log
./verify linear-output.log cmpsc311.log

echo "Testing linear.dat - Cache 64"
rm cmpsc311.log
./smsasim -c 64 linear.dat -l cmpsc311.log
./verify linear-output.log cmpsc311.log

echo "Testing linear.dat - Cache 256"
rm cmpsc311.log
./smsasim -c 256 linear.dat -l cmpsc311.log
./verify linear-output.log cmpsc311.log

echo "Testing linear.dat - Cache 1024"
rm cmpsc311.log
./smsasim linear.dat -l cmpsc311.log
./verify linear-output.log cmpsc311.log

echo "Testing linear.dat - Cache 2048"
rm cmpsc311.log
./smsasim -c 2048 linear.dat -l cmpsc311.log
./verify linear-output.log cmpsc311.log

echo "Testing linear.dat - Cache 4096"
rm cmpsc311.log
./smsasim -c 4096 linear.dat -l cmpsc311.log
./verify linear-output.log cmpsc311.log

echo "=======================================\n"

# RANDOM.DAT

echo "Testing random.dat - Cache 32"
rm cmpsc311.log
./smsasim -c 32 random.dat -l cmpsc311.log
./verify random-output.log cmpsc311.log

echo "Testing random.dat - Cache 64"
rm cmpsc311.log
./smsasim -c 64 random.dat -l cmpsc311.log
./verify random-output.log cmpsc311.log

echo "Testing random.dat - Cache 256"
rm cmpsc311.log
./smsasim -c 256 random.dat -l cmpsc311.log
./verify random-output.log cmpsc311.log

echo "Testing random.dat - Cache 1024"
rm cmpsc311.log
./smsasim random.dat -l cmpsc311.log
./verify random-output.log cmpsc311.log

echo "Testing random.dat - Cache 2048"
rm cmpsc311.log
./smsasim -c 2048 random.dat -l cmpsc311.log
./verify random-output.log cmpsc311.log

echo "Testing random.dat - Cache 4096"
rm cmpsc311.log
./smsasim -c 4096 random.dat -l cmpsc311.log
./verify random-output.log cmpsc311.log

echo "=============================================\n"

# REFLOC.DAT

echo "Testing refloc.dat - Cache 32"
rm cmpsc311.log
./smsasim -c 32 refloc.dat -l cmpsc311.log
./verify refloc-output.log cmpsc311.log

echo "Testing refloc.dat - Cache 64"
rm cmpsc311.log
./smsasim -c 64 refloc.dat -l cmpsc311.log
./verify refloc-output.log cmpsc311.log

echo "Testing refloc.dat - Cache 256"
rm cmpsc311.log
./smsasim -c 256 refloc.dat -l cmpsc311.log
./verify refloc-output.log cmpsc311.log

echo "Testing refloc.dat - Cache 1024"
rm cmpsc311.log
./smsasim refloc.dat -l cmpsc311.log
./verify refloc-output.log cmpsc311.log

echo "Testing refloc.dat - Cache 2048"
rm cmpsc311.log
./smsasim -c 2048 refloc.dat -l cmpsc311.log
./verify refloc-output.log cmpsc311.log

echo "Testing refloc.dat - Cache 4096"
rm cmpsc311.log
./smsasim -c 4096 refloc.dat -l cmpsc311.log
./verify refloc-output.log cmpsc311.log


#ifndef A2SUPPORT_INCLUDED
#define A2SUPPORT_INCLUDED

////////////////////////////////////////////////////////////////////////////////
//
//  File          : a2support.h
//  Description   : This is the header file for the functions for assignment
//                  2 of the CMPSC311 course.  Students are required to define 
//                  the following functions and implement them in another
//                  file, a2support.c.
//
//  Author        : Patrick McDaniel
//  Created       : Mon Sep  9 05:11:24 PDT 2013 
//

void printFArray( float *fArray, int length );
void printIArray( int *iArray, int length );
int myRound( float value );
void incrementFloat( float *value, float incrementBy );
void incrementInt( int *value, int incrementBy );
float largestFloat( float *fArray, int length );
int largestInt( int *iArray, int length );
void bsortFloat( float *fArray, int length );
void bsortInt( int *iArray, int length );
void printCharline( char character, int count );
void doHistogram( int *iArray, float *fArray, int intLength, int floatLength );

#endif

////////////////////////////////////////////////////////////////////////////////
//
//  File           : smsa_cache.c
//  Description    : This is the cache for the SMSA simulator.
//
//   Author        : Dylan Nguyen
//   Last Modified : 11/17/2013
//

// Include Files
#include <stdint.h>
#include <stdlib.h>

// Project Include Files
#include <smsa_cache.h>
#include <cmpsc311_log.h>
#include <cmpsc311_util.h>

// Globals
SMSA_CACHE_LINE *cache = NULL; // Array of cache lines
uint32_t num_cache_lines; // Total number of cache lines

//
// Functions

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_init_cache
// Description  : Setup the block cache
//
// Inputs       : lines - the number of cache entries to create
// Outputs      : 0 if successful test, -1 if failure

int smsa_init_cache( uint32_t lines ) {
    cache = calloc(lines, sizeof(SMSA_CACHE_LINE));
    num_cache_lines = lines;

    return 0;

}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_close_cache
// Description  : Clear cache and free associated memory
//
// Inputs       : none
// Outputs      : 0 if successful test, -1 if failure

int smsa_close_cache( void ) {
    logMessage(LOG_INFO_LEVEL, "CLOSING CACHE");

    // Free each cache line in the cache
    int i;
    for (i = 0; i < num_cache_lines; i++) {
        if (cache[i].line != NULL) {
            free(cache[i].line);
            cache[i].line = NULL;
        }
    }

    // Free the entire cache
    if (cache != NULL) {
        free(cache);
        cache = NULL;
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_get_cache_line
// Description  : Check to see if the cache entry is available
//
// Inputs       : drm - the drum ID to look for
//                blk - the block ID to lookm for
// Outputs      : pointer to cache entry if found, NULL otherwise

unsigned char *smsa_get_cache_line( SMSA_DRUM_ID drm, SMSA_BLOCK_ID blk ) {
    
    logMessage(LOG_INFO_LEVEL, "GET_CACHE_LINE: drm[%u]/blk[%u]", drm, blk);

    int i;
    for (i = 0; i < num_cache_lines; i++) {
        // If the drum and block are found in the cache, return the line.
        if (drm == cache[i].drum && blk == cache[i].block) {
            // Update used with current time.
            gettimeofday(&cache[i].used, NULL);
            logMessage(LOG_INFO_LEVEL, "GET_CACHE_LINE - Found cache for drm[%u]/blk[%u]", drm, blk);
            return cache[i].line;
        }
    }
    logMessage(LOG_INFO_LEVEL, "GET_CACHE_LINE - Cache not found for drm[%u]/blk[%u]", drm, blk);
    // If drum/block was not found in the cache, just return a NUL.
    return NULL;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : smsa_put_cache_line
// Description  : Put a new line into the cache
//
// Inputs       : drm - the drum ID to place
//                blk - the block ID to lplace
//                buf - the buffer to put into the cache
// Outputs      : 0 if successful, -1 otherwise

int smsa_put_cache_line( SMSA_DRUM_ID drm, SMSA_BLOCK_ID blk, unsigned char *buf ) {

    logMessage(LOG_INFO_LEVEL, "SMSA_PUT_CACHE_LINE: drm:[%u] blk: [%u]", drm, blk);

    int i, iLRU;
    iLRU = 0;
    for (i = 0; i < num_cache_lines; i++) {
        // First check if the drum/block is already in the current cache line.
        // If it is, then just  update the time used for that line
        if (cache[i].drum == drm && cache[i].block == blk) {
            gettimeofday(&cache[i].used, NULL);
            logMessage(LOG_INFO_LEVEL, "PUT_CACHE_LINE - Found drm[%u]/blk[%u] in cache", drm, blk);
            return 0;
        }

        // Check if there's an empty spot in the current cache line  to put a new line in.
        if (cache[i].line == NULL) {
            cache[i].drum = drm;
            cache[i].block = blk;
            cache[i].line = buf;
            gettimeofday(&cache[i].used, NULL);
            logMessage(LOG_INFO_LEVEL, "PUT_CACHE_LINE - Found empty cache line");
            return 0;
        }

        // Find index of LRU cache line
        if (compareTimes(&cache[i].used, &cache[iLRU].used) < 0) {
            iLRU = i;
        }
    }

    // Drum/block was not found in the cache, and an empty line was also not found.
    // So remove the LRU line and replace it with a new drum/block
    logMessage(LOG_INFO_LEVEL, "PUT_CACHE_LINE - Found LRU[%u]", iLRU);

    if (cache[iLRU].line != NULL) {
        free(cache[iLRU].line);
        cache[iLRU].line = NULL;
    }

    cache[iLRU].drum = drm;
    cache[iLRU].block = blk;
    cache[iLRU].line = buf;

    return 0;
}
